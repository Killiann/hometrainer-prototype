package fr.fitdev;

import java.util.LinkedList;

public class HistoryExercise {

    private int id;
    private final Exercise exercice;
    private final LinkedList<Pause> pauses;

    private final long timestampStart;

    public HistoryExercise(int id, Exercise exercice) {
        this.id = id;
        this.exercice = exercice;
        this.pauses = new LinkedList<>();
        this.timestampStart = System.currentTimeMillis();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Exercise getExercice() {
		return exercice;
	}

    public void addPause(Pause pause) {
        this.pauses.add(pause);
    }

    public LinkedList<Pause> getPauses() {
        return pauses;
    }

    public long getTimestampStart() {
        return timestampStart;
    }
    
    public long getTimestampStop() {
    	return this.pauses.isEmpty() ? -1 :this.pauses.getLast().getTimestampStop();
    }
}
