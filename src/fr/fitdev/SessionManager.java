package fr.fitdev;

import fr.fitdev.adapter.Adaptable;
import fr.fitdev.adapter.Adapter;
import fr.fitdev.adapter.AdapterManager;
import fr.fitdev.adapter.type.SessionAdapter;

public class SessionManager {
	
	private static SessionManager instance;

	private SessionAdapter sessionAdapter;
	
	private SessionManager() throws RuntimeException {
		Adapter<? extends Adaptable> adapter = AdapterManager.getAdapter(Session.class);
		if(!(adapter instanceof SessionAdapter)) {
			throw new RuntimeException("Erreur SessionAdapter corrompu");
		}

		sessionAdapter = (SessionAdapter) adapter;
	}
	
	public Session findSession(int id) {
		return sessionAdapter.find(id);
	}
	
	public void createSession(String name) throws IllegalArgumentException {
		if(!this.sessionAdapter.isNameExistAndValid(name)) {
			throw new IllegalArgumentException("Nom existe déjà");
		}
		
		Session session = new Session(-1, name);
		this.sessionAdapter.insert(session);
	}
	
	public void modifySession(int id, String name) throws IllegalArgumentException {
		if(!this.sessionAdapter.isNameExistAndValid(name)) {
			throw new IllegalArgumentException("Nom existe déjà ou est null");
		}
		
		Session session = this.findSession(id);
		if(session == null) {
			throw new IllegalArgumentException("id n'existe pas ! (S2ANCE NULL)");	
		}
		
		session.setName(name);
		this.sessionAdapter.update(session);
	}
	
	public void startSession(int id) throws IllegalArgumentException {
		Session session = this.findSession(id);
		if(session == null) {
			throw new IllegalArgumentException("id n'existe pas ! (S2ANCE NULL)");	
		}

		History history = new History();
		session.start(history);
		session.addHistory(history);
	}
	
	public void archiveSession(int id) throws IllegalArgumentException {
		Session session = this.findSession(id);
		if(session == null) {
			throw new IllegalArgumentException("id n'existe pas ! (S2ANCE NULL)");	
		}
		
		session.setActive(false);
		this.sessionAdapter.update(session);
	}
	
	
	public static SessionManager getInstance() {
		return instance == null ? instance = new SessionManager() : instance;
	}
}
