package fr.fitdev;

import java.util.LinkedList;
import java.util.TreeSet;

import fr.fitdev.adapter.Adaptable;

public class Session implements Adaptable {

    private int id;
    private String name;
    private boolean active;

    private final TreeSet<Component> components;
    private final LinkedList<History> histories;

    public Session(int id, String name) {
        this.id = id;
        this.name = name;
        this.components = new TreeSet<>();
        this.histories = new LinkedList<>();
    }
    
    public void start(History history) {
    	//TODO: Lancement de la task ou de la boucle réflective pour 
    }

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
		this.id = id;
	}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void addComponent(Component component) {
        this.components.add(component);
    }

    public TreeSet<Component> getComponents() {
        return components;
    }
    
    public void addHistory(History history) {
    	this.histories.add(history);
    }
    
    public LinkedList<History> getHistories() {
		return histories;
	}
}
