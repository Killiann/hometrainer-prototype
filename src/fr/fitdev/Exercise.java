package fr.fitdev;

public class Exercise extends Component {

	private String name;
    private int duration;
    private int power;
    private int speed;

    public Exercise(int id, int repetition, int order, String name, int duration, int power, int speed) {
        super(id, repetition, order);
        this.name = name;
        this.duration = duration;
        this.power = power;
        this.speed = speed;
    }

    @Override
    public void start(History history) {
        history.addHistory(new HistoryExercise(-1, this));
    }

    public String getName() {
        return name;
    }

    public int getDuration() {
        return duration;
    }

    public int getPower() {
        return power;
    }

    public int getSpeed() {
        return speed;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

	@Override
	public boolean isValid() {
		return true; //TODO:
	}
}
