package fr.fitdev.adapter;

import java.util.HashMap;
import java.util.Map;

import fr.fitdev.Exercise;
import fr.fitdev.Group;
import fr.fitdev.Session;
import fr.fitdev.adapter.type.ExerciseAdapter;
import fr.fitdev.adapter.type.GroupAdapter;
import fr.fitdev.adapter.type.SessionAdapter;

public class AdapterManager {

	private static AdapterManager instance;
	
	private final Map<Class<? extends Adaptable>, Adapter<? extends Adaptable>> adapters;
	
	private AdapterManager() {
		adapters = new HashMap<>();

		adapters.put(Exercise.class, new ExerciseAdapter());
		adapters.put(Group.class, new GroupAdapter());
		adapters.put(Session.class, new SessionAdapter());
	}
	
	public static Adapter<Adaptable> getAdapter(Class<? extends Adaptable> clazz) {
		if(instance == null) {
			instance = new AdapterManager();
		}
		
		return (Adapter<Adaptable>) instance.adapters.get(clazz); //Possibilité d'amélioration
	}
}
