package fr.fitdev.adapter.type;

import fr.fitdev.Exercise;
import fr.fitdev.Session;
import fr.fitdev.adapter.ComponentAdapter;

public class ExerciseAdapter extends ComponentAdapter<Exercise> {

	@Override
	public void insert(Exercise generic, Session session) {
	}

	@Override
	public void update(Exercise generic) {
	}

	@Override
	public Exercise find(int key) {
		return null;
	}

	@Override
	public void delete(Exercise generic) {
	}
}
