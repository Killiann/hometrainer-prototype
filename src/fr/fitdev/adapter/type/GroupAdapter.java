package fr.fitdev.adapter.type;

import fr.fitdev.Group;
import fr.fitdev.Session;
import fr.fitdev.adapter.ComponentAdapter;

public class GroupAdapter extends ComponentAdapter<Group> {

	@Override
	public void insert(Group generic, Session session) {
	}

	@Override
	public void update(Group generic) {
	}

	@Override
	public Group find(int key) {
		return null;
	}

	@Override
	public void delete(Group generic) {
	}
}
