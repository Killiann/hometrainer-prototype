package fr.fitdev.adapter;

public interface Adapter<T extends Adaptable> {

	void insert(T generic);
	
	void update(T generic);
	
	void delete(T generic);
	
	T find(int key);
}
