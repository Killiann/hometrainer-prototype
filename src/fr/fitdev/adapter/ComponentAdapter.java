package fr.fitdev.adapter;

import fr.fitdev.Session;

public abstract class ComponentAdapter<T extends Adaptable> implements Adapter<T> {

    @Override @Deprecated
    public void insert(Adaptable generic) {
        throw new UnsupportedOperationException("insert sans Session n'est pas autorisé");
    }

    public abstract void insert(T generic, Session session);
}
