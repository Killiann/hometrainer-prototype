package fr.fitdev;

import java.util.ArrayList;
import java.util.List;

public class Group extends Component {

    private List<Exercise> exercises;

    public Group(int id, int repetition, int order) {
        super(id, repetition, order);
        this.exercises = new ArrayList<>();
    }

    @Override
    public void start(History history) {
        //TODO: Lancer que le 1er, le reste en file d'attente
    }

    public void addExercise(Exercise exercise) {
        this.exercises.add(exercise);
    }

    public List<Exercise> getExercises() {
        return exercises;
    }

	@Override
	public boolean isValid() {
		return this.exercises.stream().allMatch(Component::isValid);
	}
}
