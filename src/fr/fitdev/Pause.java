package fr.fitdev;

public class Pause {

    private final long timestampStart;
    private final long timestampStop;

    public Pause(long timestampStart, long timestampStop) {
        this.timestampStart = timestampStart;
        this.timestampStop = timestampStop;
    }

    public long getTimestampStart() {
        return timestampStart;
    }

    public long getTimestampStop() {
        return timestampStop;
    }
}
