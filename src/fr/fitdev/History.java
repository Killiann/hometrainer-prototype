package fr.fitdev;

import java.util.LinkedList;

public class History {

    private LinkedList<HistoryExercise> histories;

    public History() {
    	this.histories = new LinkedList<>();
	}
    
    public long getStart() {
    	return this.histories.getFirst().getTimestampStart();
    }
    
    public long getTotalDuration() {
    	return this.histories.getLast().getTimestampStop() - this.getStart();
    }

    public void addHistory(HistoryExercise history) {
        this.histories.add(history);
    }

    public LinkedList<HistoryExercise> getHistories() {
        return histories;
    }
}
