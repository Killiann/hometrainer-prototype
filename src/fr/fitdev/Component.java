package fr.fitdev;

import fr.fitdev.adapter.Adaptable;

public abstract class Component implements Adaptable, Comparable<Component> {

    protected int id;
    protected int repetition;
    protected int order;

    public Component(int id, int repetition, int order) {
        this.id = id;
        this.repetition = repetition;
        this.order = order;
    }

    public int getRepetition() {
        return repetition;
    }

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
		this.id = id;
	}

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public void setRepetition(int repetition) {
        this.repetition = repetition;
    }

    @Override
    public int compareTo(Component o) {
        return o.order-this.order;
    }

    public abstract void start(History history);
    
    public abstract boolean isValid();
}
