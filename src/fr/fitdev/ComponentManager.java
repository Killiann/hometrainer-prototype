package fr.fitdev;

import fr.fitdev.adapter.Adaptable;
import fr.fitdev.adapter.Adapter;
import fr.fitdev.adapter.AdapterManager;
import fr.fitdev.adapter.ComponentAdapter;

public class ComponentManager {
	
	private static ComponentManager instance;
	
	private ComponentManager() {
		
	}
	
	public Component findComponent(int id, Class<? extends Component> clazz) {
		return (Component) AdapterManager.getAdapter(clazz).find(id);
	}
	
	public void createComponent(Component component, Session session) throws IllegalArgumentException {
		if(!component.isValid()) {
			throw new IllegalArgumentException("Component non valide");
		}

		this.getComponentAdapter(component.getClass()).insert(component, session);
		session.addComponent(component);
	}
	
	public void modifyComponent(Component component) {
		if(!component.isValid()) {
			throw new IllegalArgumentException("Component non valide");
		}

		this.getComponentAdapter(component.getClass()).update(component);
	}
	
	public void deleteComponent(Component component) throws IllegalArgumentException {
		if(!component.isValid()) {
			throw new IllegalArgumentException("Component non valide");
		}
		
		if(component instanceof Group) {
			Group group = (Group) component;
			
			for (Exercise exercise : group.getExercises()) {
				try {
					this.deleteComponent(exercise);
				} catch (IllegalArgumentException e) {
					//TODO: Comment la g�rer ? Arreter l�, mettre une erreur � l'�cran ?
				}
			}
		}
		
		component.setOrder(-1);
		this.getComponentAdapter(component.getClass()).update(component);
	}
	
	private ComponentAdapter<Adaptable> getComponentAdapter(Class<? extends Component> clazz) throws IllegalArgumentException {
		Adapter<Adaptable> adapter = AdapterManager.getAdapter(clazz);
		if(!(adapter instanceof ComponentAdapter)) {
			throw new IllegalArgumentException("Component corrompu");
		}

		return (ComponentAdapter<Adaptable>) adapter;
	}
	
	public static ComponentManager getInstance() {
		return instance == null ? instance = new ComponentManager() : instance;
	}

}
